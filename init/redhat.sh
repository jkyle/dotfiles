#!/bin/bash

rpm -ivh http://yum.puppetlabs.com/el/6/products/i386/puppetlabs-release-6-7.noarch.rpm
yum install git puppet -y

cd ~
git clone https://bitbucket.org/jkyle/dotfiles.git .dotfiles

cd .dotfiles
sudo puppet apply manifests/default.pp