#!/bin/bash

cd /tmp/
wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb
sudo dpkg -i puppetlabs-release-precise.deb
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get install curl git puppet -y

cd ~
git clone https://bitbucket.org/jkyle/dotfiles.git .dotfiles

cd .dotfiles
sudo puppet apply manifests/default.pp
