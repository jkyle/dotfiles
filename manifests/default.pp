import 'packages.pp'
import 'user.pp'
import 'vim.pp'
import 'git.pp'
import 'tmux.pp'
import 'readline.pp'

class setup(
  $repo = 'https://bitbucket.org/jkyle/dotfiles.git',
  $user = 'jkyle',
) {
  include packages
  class {'tmux':     require => Exec['setup-home'] } 
  class {'readline': require => Exec['setup-home'] } 
  class {'vim':      require => Exec['setup-home'] }
  class {'git':      require => Exec['setup-home'] }
  class {'user':
    user => $user,
    gid  => $user,
  }

  file {'/tmp/setup.sh':
    mode     => 0755,
    owner    => root,
    group    => root,
    content  => "
#!/bin/bash

/usr/bin/git clone -b develop --depth 1 ${repo} .dotfiles 
cd .dotfiles
/usr/bin/git submodule init
/usr/bin/git submodule update

ausr/bin/git submodule init
/usr/bin/git submodule update
/usr/bin/git submodule foreach git submodule init
/usr/bin/git submodule foreach git submodule update
",
  }
 
  $zrc = ['zlogin', 'zlogout', 'zpreztorc', 'zprofile', 'zshenv', 'zshrc']
  zrc::link {$zrc: user => $user }

  exec {"setup-home":
    cwd      => "/home/${user}",
    command  => "/tmp/setup.sh",
    creates  => "/home/${user}/.dotfiles",
    user     => $user,
    provider => "shell",
    require  => [Package['git'], File['/tmp/setup.sh'], User[$user]],
  }

  exec {'get-devstack':
    cwd     => "/home/${user}",
    creates => "/home/${user}/devstack",
    user    => $user,
    command => "/usr/bin/git clone https://github.com/openstack-dev/devstack.git",
    require => [Package['git'], User[$user]],
  }

  file {"/home/${user}/devstack/localrc":
    mode    => 0644,
    owner   => $user,
    group   => $user,
    content => "# vim: set ft=sh
disable_service n-net
enable_service q-svc
enable_service q-agt
enable_service q-dhcp
enable_service q-l3
enable_service q-meta
enable_service quantum
#Optional, to enable tempest configuration as part of devstack
enable_service tempest
DATABASE_PASSWORD=demo
RABBIT_PASSWORD=demo
SERVICE_TOKEN=demo
SERVICE_PASSWORD=demo
ADMIN_PASSWORD=demo
  ",
    require => Exec['get-devstack'],
  }

  file {"/home/${user}/.zprezto":
    ensure  =>  link,
    owner   => $user,
    group   => $user,
    target  => "/home/${user}/.dotfiles/config.d/zprezto",
    require =>  Exec['setup-home'],
  }
}

define zrc::link ($user) {
  $home = "/home/${user}"
  $dots = "${home}/.dotfiles"

  file {"${home}/.${title}":
    ensure  =>  link,
    owner   => $user,
    group   => $user,
    target  => "${home}/.zprezto/runcoms/${title}",
    require => File["/home/${user}/.zprezto"]
  }
}

class {'setup': }
