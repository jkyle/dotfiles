class git (
  $user = 'jkyle',
  $git_source = "/home/jkyle/.dotfiles/config.d/git",
){
  file {"/home/${user}/.gitconfig":
    ensure  =>  link,
    owner   => $user,
    group   => $user,
    target  => "${git_source}/gitconfig",
  }

  file {"/home/${user}/.gitignore":
    ensure  =>  link,
    owner   => $user,
    group   => $user,
    target  => "${git_source}/gitignore",
  }
}
