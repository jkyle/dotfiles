class packages {
  $groups = $::osfamily ? {
    "RedHat" => ['adm', 'wheel'],
    "Debian" => ['adm', 'sudo'],
    "Darwin" => ['staff'],
  }

  $packages = $::osfamily ? {
    "RedHat" => ['zsh', 'git'],
    "Debian" => [
                 'zsh', 
                 'git', 
                 'git-flow', 
                 'virtualenvwrapper', 
                 'python-pip', 
                 'git-review'],
    "Darwin" => ['zsh', 'git', 'git-flow']
  }

  if $::osfamily == "RedHat" {
    $gitflow = "https://raw.github.com/nvie/gitflow/develop/contrib/gitflow-installer.sh" 
    exec {'get-gitflow-installer':
      cwd     => "/tmp",
      command => "/usr/bin/curl -O ${gitflow}",
      creates => "/tmp/gitflow-installer.sh",
    }
    exec {'install-gitflow':
      cwd     =>  '/tmp',
      command => "/bin/bash gitflow-installer.sh",
      require => Exec['get-gitflow-installer'],
    }
  }

  package {$packages: }

}


