class readline (
  $user            = 'jkyle',
  $readline_source = "/home/jkyle/.dotfiles/config.d/shell",
){
  file {"/home/${user}/.editrc":
    ensure  => link,
    owner   => $user,
    group   => $user,
    target  => "${readline_source}/editrc",
  }
  file {"/home/${user}/.inputrc":
    ensure  => link,
    owner   => $user,
    group   => $user,
    target  => "${readline_source}/inputrc",
  }
}
