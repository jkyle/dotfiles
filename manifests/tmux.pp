class tmux (
  $user        = 'jkyle',
  $tmux_source = "/home/jkyle/.dotfiles/config.d/tmux",
){
  if $::osfamily == "Darwin" {
    $os_family = 'tmux.osx.conf'
    
    file {"/home/${user}/.tmux.local.conf":
      ensure  => link,
      owner   => $user,
      group   => $user,
      target  => "${tmux_source}/${os_conf}",
    }

  }


  file {"/home/${user}/.tmux.conf":
    ensure  => link,
    owner   => $user,
    group   => $user,
    target  => "${tmux_source}/tmux.conf",
  }
}
