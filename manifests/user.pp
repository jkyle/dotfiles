class user (
  $user                   = 'jkyle',
  $gid                    = 'jkyle',
  $authorized_key_comment = 'jkyle@Air-Protoss.local',
  $authorized_key_type    = "ssh-rsa",
  $authorized_key = "AAAAB3NzaC1yc2EAAAADAQABAAABAQC/KTbhKZEL19BhHovHamtMLJNv85nC1GKpRvp3PAI1xEvze63cTwC43HqbvBJEWIyMQYTZDfHxujJSxy2KMvwU96fVhxQbA+SieMskxjlujXiU0WAwIlzo+5dZhgdMwTX0YUD5NwkVoELG/wAFiOZ0f4gbjOK8ossHGGNWGYl5b/pFbzb4ih2y+3LM80s8KBj3CF5bJS57fkhiYAwPIyWnqMgcaVsAcfL7XXPZsabzH9eVMmngfwimMkn0wzujuGrtw+bFGoEfDyD2HeY/bEn7eoBO9AMiyR25a5fIYvniRiOKCNkzSQeZfj5S45+SS82YZc7qtuZzRPmNyVkgFDLx",

) {
  user { $user:
    ensure      => present,
    uid         => 5000,
    gid         => $gid,
    groups      => $groups,
    shell       => '/bin/zsh',
    home        => "/home/$user",
    managehome  => true,
    password    => '*',
    require     => Group[$user],
  }

  group {$user: gid => 5000 }
  ssh_authorized_key {$authorized_key_comment:
    user    => $user,
    ensure  => present,
    type    => $authorized_key_type,
    key     => $authorized_key,
    require => User[$user],
  }

  file {"/etc/sudoers.d/${user}":
    mode => 0440,
    owner => root,
    group => root,
    content => "${user} ALL = (ALL) NOPASSWD: ALL",
  }
}
