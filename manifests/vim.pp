class vim (
  $user = 'jkyle',
  $vim_conf = "/home/jkyle/.dotfiles/config.d/dotvim",
){
  file {"/home/${user}/.vim":
    ensure  =>  link,
    owner   => $user,
    group   => $user,
    target  => $vim_conf,
    require =>  Exec['setup-home'],
  }

  file {"/home/${user}/.vimrc":
    ensure  =>  link,
    owner   => $user,
    group   => $user,
    target  => "/home/${user}/.vim/vimrc",
    require =>  File["/home/${user}/.vim"],
  }
}

